import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTraceComponent } from './home-trace.component';

describe('HomeTraceComponent', () => {
  let component: HomeTraceComponent;
  let fixture: ComponentFixture<HomeTraceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeTraceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTraceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
